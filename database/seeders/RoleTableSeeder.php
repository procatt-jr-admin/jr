<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'Developer';
        $role_user->slug = 'developer';
        $role_user->save();

        $role_author = new Role();
        $role_author->name = 'Recruiter';
        $role_author->slug = 'recruiter';
        $role_author->save();
    }
}
