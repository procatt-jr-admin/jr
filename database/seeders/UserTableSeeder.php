<?php

namespace Database\Seeders;

use Database\Factories\UserDataFactory;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserData;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_developer = Role::where('slug', 'developer')->first();
        $developer = new User();
        $developer->name = 'Alexander';
        $developer->second_name = 'Chabanov';
        $developer->email = 'oleksandr.chabanov@gmail.com';
        $developer->phone = '+380679602238';
        $developer->password = bcrypt('alexandr');
        $developer->save();
        $developer->data()->save(UserData::factory()->make());
        $developer->roles()->attach($role_developer);

        $role_recruiter = Role::where('slug', 'recruiter')->first();
        $recruiter = new User();
        $recruiter->name = 'Roman';
        $recruiter->second_name = 'R.';
        $recruiter->email = 'roman@example.com';
        $recruiter->phone = '+380679602239';
        $recruiter->password = bcrypt('roman');
        $recruiter->save();
        $recruiter->data()->save(UserData::factory()->make());
        $recruiter->roles()->attach($role_recruiter);
    }
}
