import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/home',
        name: 'Home',
        component: () => import(/* webpackChunkName: "about" */ '../pages/Home.vue')
    },
    {
        path: '/lk',
        name: 'Lk',
        component: () => import(/* webpackChunkName: "about" */ '../pages/Lk.vue'),
        children: [
            {
                path: '/lk/profile',
                name: 'Profile',
                component: () => import(/* webpackChunkName: "about" */ '../pages/Profile.vue')
            },
            {
                path: '/lk/questionnaire',
                name: 'Questionnaire',
                component: () => import(/* webpackChunkName: "about" */ '../pages/Questionnaire.vue')
            },
            {
                path: '/lk/notification',
                name: 'Notification',
                component: () => import(/* webpackChunkName: "about" */ '../pages/Notification.vue')
            },
            {
                path: '/lk/responses',
                name: 'Responses',
                component: () => import(/* webpackChunkName: "about" */ '../pages/Responses.vue')
            },
            {
                path: '/lk/analytics',
                name: 'Analytics',
                component: () => import(/* webpackChunkName: "about" */ '../pages/Analytics.vue')
            },
            {
                path: '/lk/contacts',
                name: 'Contacts',
                component: () => import(/* webpackChunkName: "about" */ '../pages/Contacts.vue')
            },
        ]
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
