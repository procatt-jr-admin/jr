import axios from "axios";
import {
    SET_USER
} from "./mutation-types"

export default {
    async LOGIN(context, payload) {
        let result = {};
        try {
            const { data } = await axios.post(
                context.state.serverURL + "/api/sanctum/token",
                payload
            );
            if (data.success) {
                context.commit(SET_USER, data.user);
                result = {
                    success: true,
                };
            }
        } catch (e) {
            result = {
                authError: e.response.data.message,
                success: false,
            };
        }

        return result;
    },
}
