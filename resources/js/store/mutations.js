import {
    SET_USER,
    SET_THEME
} from "./mutation-types"

export default {
    [SET_USER](state, payload) {
        state.user = payload;
    },
    [SET_THEME](state, payload) {
        state.theme = payload;
    },
}
